package test.java.com.overtheinfinite.nationbuilder.balance.properties;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.com.overtheinfinite.nationbuilder.balance.properties.Properties;

public class PropertiesTest {
	@Test
	public void testProperties() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Properties ab = mapper.readValue(new File("properties/culture_people.json"), 
				Properties.class);
		ab.setName("culture_people");
		System.out.println(ab);
	}
}
