package test.java.com.overtheinfinite.nationbuilder.balance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.com.overtheinfinite.nationbuilder.balance.RowConverter;

public class RowConverterTest {
	@Test
	public void testBalance() throws JsonParseException, JsonMappingException, IOException {		
		ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		ArrayList<String> sqls = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		list = mapper.readValue(new File("balance/culture_people.json"), list.getClass());
		System.out.println(list);
		RowConverter rc = new RowConverter();
		
		rc.setName("culture_people");
		for(int i = 0; i < list.size(); i++) {
			String sql = rc.convert(list.get(i));
			System.out.println(sql);
			sqls.add(sql);
		}
	}
}
