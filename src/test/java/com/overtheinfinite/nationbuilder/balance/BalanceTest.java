package test.java.com.overtheinfinite.nationbuilder.balance;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import main.java.com.overtheinfinite.nationbuilder.balance.Balance;

public class BalanceTest {
	@Test
	public void loadBalance() throws JsonParseException, JsonMappingException, IOException, SQLException {
		Balance b = Balance.getInstance();
		b.load("culture_people.json", "culture_people");
		b.load("culture.json", "culture");
		b.load("people.json", "people");
		b.load("policy_people.json", "policy_people");
		b.load("policy.json", "policy");
	}
}
