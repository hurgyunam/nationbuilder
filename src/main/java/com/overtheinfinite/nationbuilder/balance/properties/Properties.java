package main.java.com.overtheinfinite.nationbuilder.balance.properties;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @var name : tablename
 * @func toString() : all properties data make create table sql
 * @author heokyunam
 *
 */
public class Properties extends ArrayList<Property> {
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	/**
	 * make complete create table statements
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("create table ");
		sb.append(name);
		sb.append("(");
		
		sb.append(this.get(0));
		
		for(int i = 1; i < this.size(); i++) {
			sb.append(",");
			sb.append(this.get(i));
		}
		
		sb.append(");");
		return sb.toString();
	}
}