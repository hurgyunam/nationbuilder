package main.java.com.overtheinfinite.nationbuilder.balance.properties;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @var name, type, index
 * @var defaultValue number default value
 * @var defaultText text default value
 * @func toString : make part of create table sql
 * @author heokyunam
 *
 */
public class Property {
	private String name;
	private String type;
	private boolean index;
	private Integer defaultValue;
	private String defaultText;
	
	public String getDefaultText() {
		return defaultText;
	}
	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isIndex() {
		return index;
	}
	public void setIndex(boolean index) {
		this.index = index;
	}
	public void setDefault(Integer value) {
		this.defaultValue = value;
	}
	public Integer getDefault() {
		return this.defaultValue;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("'");
		sb.append(name);
		sb.append("' ");
		sb.append(type);
		if(isIndex()) {
			//index -> primary key & auto increment
			sb.append(" primary key ");
		}
		if(getDefault() != null) {
			//default integer
			sb.append(" default ");
			sb.append(getDefault());
		}
		if(getDefaultText() != null) {
			//default text
			sb.append(" default '");
			sb.append(getDefaultText());
			sb.append("'");
		}
		return sb.toString();
	}
}
