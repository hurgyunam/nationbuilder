package main.java.com.overtheinfinite.nationbuilder.balance;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.com.overtheinfinite.nationbuilder.balance.properties.Properties;

public class Balance {
	private JSONDBHelper helper;
	public Balance() {
		helper = new JSONDBHelper("\\balance.db");
	}
	
	/**
	 * load json and save data that we need
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void load() throws JsonParseException, JsonMappingException, IOException {
		helper.load("culture_people");
		helper.load("culture");
		helper.load("people");
		helper.load("policy_people");
		helper.load("policy");
	}
	
	
	private static Balance balance;
	public static Balance getInstance() {
		if(balance == null) {
			balance = new Balance();
		}
		return balance;
	}

}
