package main.java.com.overtheinfinite.nationbuilder.balance;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * @var name : tablename
 * @func convert : 해쉬테이블을 입력해 insert문으로 반환
 * @author heokyunam
 *
 */
public class RowConverter {
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String convert(HashMap<String, Object> table) {
		StringBuilder keys = new StringBuilder();
		StringBuilder values = new StringBuilder();
		
		Iterator<String> it = table.keySet().iterator();
		String key = it.next();
		keys.append(key);
		values.append("'");
		values.append(table.get(key));
		values.append("'");
		
		while(it.hasNext()) {
			key = it.next();
			keys.append(',');
			values.append(',');
			
			keys.append(key);
			values.append("'");
			values.append(table.get(key));
			values.append("'");
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("insert into ");
		sb.append(name);
		sb.append(" (");
		sb.append(keys);
		sb.append(") values (");
		sb.append(values);
		sb.append(");");
		return sb.toString();
	}
}
