package main.java.com.overtheinfinite.nationbuilder.balance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * SQLite base class
 * @author heokyunam
 *
 */
public class DBHelper {
	private Connection connection;
	private String dbName;
	private boolean isOpened = false;
	
	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public DBHelper(String dbName) {
		File file = new File("");
		this.dbName = file.getAbsolutePath() + "" + dbName;
		open();
	}
	
	public boolean open() {
		try {
			this.connection = DriverManager.getConnection(
					"jdbc:sqlite:/" + this.dbName);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		this.isOpened = true;
		return true;
	}
	
	public boolean close() {
		if(this.isOpened == false) return true;
		
		try {
			this.connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public PreparedStatement execute(String sql) throws SQLException {
		if(this.isOpened == false) return null;
		return this.connection.prepareStatement(sql);
	}
	
	public boolean delete() {
		Path path = Paths.get(dbName);
		try {
			Files.delete(path);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean exist() {
		File file = new File(dbName);
		boolean result = file.exists();
		return result;
	}
}
