package main.java.com.overtheinfinite.nationbuilder.balance;

import java.io.IOException;
import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SaveManager {
	private JSONDBHelper helper;
	public SaveManager() {
		helper = new JSONDBHelper("\\save.db");
	}
	//load는 단순히 properties만 로드
	//그런데 시티즌같은 경우에는 어떻게 처리되려나? 일단 디폴트 쓰자
	public void load() throws JsonParseException, JsonMappingException, IOException {
		try {
			helper.loadProperty("citizen.json", "citizen");
			helper.loadProperty("savedata.json", "savedata");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static SaveManager instance;
	public static SaveManager getInstance() {
		if(instance == null) {
			instance = new SaveManager();
		}
		return instance;
	}
}
