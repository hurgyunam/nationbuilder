package main.java.com.overtheinfinite.nationbuilder.balance;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.com.overtheinfinite.nationbuilder.balance.properties.Properties;

public class JSONDBHelper extends DBHelper {
	public JSONDBHelper(String name) {
		super(name);
	}
	
	public void load(String tablename) throws JsonParseException, JsonMappingException, IOException {
		String filename = tablename + ".json";
		load(filename, filename, tablename);
	}
	
	public void load(String filename, String tablename) throws JsonParseException, JsonMappingException, IOException {
		load(filename, filename, tablename);
	}
	
	public void loadProperty(String propertyFile, String tablename) throws JsonParseException, JsonMappingException, IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		//load propertyFile
		File f = new File("properties/" + propertyFile);
		if(!f.exists()) 
			throw new IllegalArgumentException("property file is not exists");
		Properties p = (Properties) mapper.readValue(f, Properties.class);
		p.setName(tablename);
		String createSQL = p.toString();
		this.execute(createSQL).execute();
	}
	
	public void loadBalance(String balanceFile, String tablename) throws JsonParseException, JsonMappingException, IOException, SQLException {
		List<String> sqls = null;
		//load balance file
		sqls = loadData(balanceFile, tablename);
		for(int i = 0; i < sqls.size(); i++) {
			this.execute(sqls.get(i)).execute();
		}
	}
	/**
	 * make create table statements and insert statements.
	 * @param filename json file name, not folder
	 * @param tablename table name what you wanna make
	 * @throws SQLException print error spec and skip
	 */
	public void load(String propertyFile, String balanceFile, String tablename) throws JsonParseException, JsonMappingException, IOException {
		try
		{
			loadProperty(propertyFile, tablename);
			loadBalance(balanceFile, tablename);
		}catch(SQLException e) {
			//if table already exists error it is ok
			String msg = e.toString();
			System.out.println(msg);
		}
	}
	
	/**
	 * @param filename json file name, not folder
	 * @param tablename table name that you wanna make
	 * @return json data convert into insert statements 
	 */
	public List<String> loadData(String filename, String tablename) throws JsonParseException, JsonMappingException, IOException {
		//load balance file
		ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		ArrayList<String> sqls = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		list = mapper.readValue(new File("balance/" + filename), list.getClass());
		
		//read balance data to hashtable and it make hashtable to sql
		//cuz it doesn't work if we use self-made class
		RowConverter rc = new RowConverter();
		rc.setName(tablename);
		for(int i = 0; i < list.size(); i++) {
			String sql = rc.convert(list.get(i));
			sqls.add(sql);
		}
		return sqls;
	}
}
