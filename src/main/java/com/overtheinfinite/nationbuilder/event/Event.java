package main.java.com.overtheinfinite.nationbuilder.event;

/**
 * trigger event
 * checking changes
 * case 1. only check value
 * case 2. check condition also
 * 
 * cond 1. tutorial
 * cond 2. button action
 * cond 3. spinner change
 * @author heokyunam
 *
 */
public interface Event {
	public void onChanged(Object value);
}
